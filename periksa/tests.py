from django.test import TestCase, Client
from .models import PengunjungPeriksa
from .views import index_periksa, hasil_periksa
from django.urls import reverse

# Create your tests here.
class TestModels(TestCase):
    def setUp(self):
        self.pengunjung_periksa_test = PengunjungPeriksa.objects.create(
            nama = "Pengunjung Periksa Test",
            daerah = "DKI Jakarta"
        )
    
    def test_nama_pengunjung_periksa(self):
        self.assertEquals(str(self.pengunjung_periksa_test), "Pengunjung Periksa Test")

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.index_periksa_url = reverse("periksa:index_periksa")
        self.hasil_periksa_url = reverse("periksa:hasil_periksa")

    def test_index_periksa_GET(self):
        response = self.client.get(self.index_periksa_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "periksa/index.html")
        self.assertContains(response, "Periksa Gejala Anda")

    def test_hasil_periksa_GET(self):
        response = self.client.get(self.hasil_periksa_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "periksa/hasil_periksa.html")
        self.assertContains(response, "Hasil Periksa")

    def test_hasil_periksa_POST_adds_new_pengunjung_periksa(self):
        response = self.client.post(self.hasil_periksa_url, {"nama" : "Pengunjung", "daerah" : "DKI Jakarta", "gejala" : "1"})
        self.assertEquals(response.status_code, 200)
        self.assertEquals(PengunjungPeriksa.objects.all()[0].nama, "Pengunjung")
    
    def test_hasil_periksa_POST_no_choice_taken(self):
        # Karena pilihannya gaada, jadi tidak ada argumen 'gejala'
        response = self.client.post(self.hasil_periksa_url, {"nama" : "Pengunjung", "daerah" : "DKI Jakarta"})
        self.assertEquals(response.status_code, 302)

    def test_hasil_periksa_POST_contradiction_choice(self):
        response = self.client.post(self.hasil_periksa_url, {"nama" : "Pengunjung", "daerah" : "DKI Jakarta", "gejala" : ("1", "13")})
        self.assertEquals(response.status_code, 302)

    def test_hasil_periksa_POST_no_symptomps(self):
        response = self.client.post(self.hasil_periksa_url, {"nama" : "Pengunjung", "daerah" : "DKI Jakarta", "gejala" : "13"})
        self.assertContains(response, "Peluang Pengunjung terkena COVID-19 adalah 0%")
    
    def test_hasil_periksa_POST_small_chance(self):
        response = self.client.post(self.hasil_periksa_url, {"nama" : "Pengunjung", "daerah" : "DKI Jakarta", "gejala" : ("1", "2")})
        self.assertContains(response, "Peluang Pengunjung terkena COVID-19 adalah 16.667%")

    def test_hasil_periksa_POST_medium_chance(self):
        response = self.client.post(self.hasil_periksa_url, {"nama" : "Pengunjung", "daerah" : "DKI Jakarta", "gejala" : ("1", "2", "3", "4", "5", "6")})
        self.assertContains(response, "Peluang Pengunjung terkena COVID-19 adalah 50.0%")

    def test_hasil_periksa_POST_large_chance(self):
        response = self.client.post(self.hasil_periksa_url, {"nama" : "Pengunjung", "daerah" : "DKI Jakarta", "gejala" : ("1", "2", "3", "4", "5", "6", "7", "8", "9", "10")})
        self.assertContains(response, "Peluang Pengunjung terkena COVID-19 adalah 83.333%")
