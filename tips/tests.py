from django.test import TestCase, Client
from .models import tips


# Create your tests here.
class MainTestCase(TestCase):
    def setUp(self):
        self.c = Client
    def test_root_url_status_200(self):
        response = Client().get('/tips/')
        self.assertEqual(response.status_code, 200)
        
        
    
    def test_template_yang_dipake(self):
        response = Client().get('/tips/')
        self.assertTemplateUsed(response, "tips.html")
        
        
    def test_buat_tips_baru(self):
        tips_test = tips.objects.create(pesan = "cuci tangan")
        count = tips.objects.all().count()
        self.assertEqual(count,1)
        self.assertEqual(str(tips_test), "cuci tangan")
    
    def test_form_ada(self):
        response = Client().get("/")
        self.assertContains(response, "")

    def test_form_ada_hasilnya(self):
        arg = {'pesan': 'ini_pesan'}
        Client().post('/tips/',data = arg)
        amm_post = tips.objects.all().count()
        self.assertEqual(amm_post,1)
        response = Client().get("/")
        self.assertContains(response,"Ini_pesan")

        


    