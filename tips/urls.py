from django.urls import path

from . import views
# from main.views import home

app_name = 'tips'

urlpatterns = [
    path('', views.tips, name = 'tips')
]