from django.shortcuts import render, redirect
from . import forms,models

# Create your views here.
def tips(request):
    if request.method == "POST":
        formulir = forms.Tips(request.POST)
        if formulir.is_valid():
            all_tips = models.tips()
            all_tips.pesan = formulir.cleaned_data['pesan']
            all_tips.save()
        return redirect("/")
    else:
        formulir = forms.Tips()
        isi_pesan = {'tips':formulir}
    return render(request,'tips.html',isi_pesan)