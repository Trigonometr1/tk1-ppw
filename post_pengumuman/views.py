from django.contrib import messages
from django.db import IntegrityError
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from .models import Pengumuman, Pemerintah
from .forms import PengumumanForm

# Create your views here.
def home(request):
    return render(request, 'post_pengumuman/post_home.html')

def form(request, string):
    pemerintah_obj, created = Pemerintah.objects.get_or_create(provinsi = string)
    if request.method == 'POST':
        form = PengumumanForm(request.POST)
        if form.is_valid():
            pengumuman_obj = form.save(commit=False)
            pengumuman_obj.pemerintah = pemerintah_obj
            try:
                pengumuman_obj.save()
            except IntegrityError as e:
                messages.add_message(request, messages.ERROR, 'Gagal: Duplikat pengumuman dengan judul dan prioritas yang sama.')
                return HttpResponseRedirect('/post-pengumuman/{}/'.format(string))
            return HttpResponseRedirect('/post-pengumuman/{}/sukses'.format(string))
    else:
        form = PengumumanForm()
    return render(request, 'post_pengumuman/post_form.html', {'provinsi':str(pemerintah_obj), 'form':form, 'kode_prov':string})

def sukses(request, string):
    pemerintah = Pemerintah.objects.get(provinsi = string)
    message = 'Pengumuman untuk Provinsi {} berhasil ditambahkan. Kembali ke halaman utama untuk melihatnya.'.format(str(pemerintah))
    return render(request, 'post_pengumuman/post_home.html', {'message':message, 'provinsi':str(pemerintah), 'kode_prov':string})
