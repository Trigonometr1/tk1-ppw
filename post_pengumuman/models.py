from django.db import models
from django.utils.text import slugify

# Create your models here.
class Pemerintah(models.Model):
    PROVINSI_CHOICES = (
        ('BAN','Banten'),
        ('JKT','DKI Jakarta'),
        ('JBR','Jawa Barat'),
        ('JTE','Jawa Tengah'),
        ('YOG','DI Yogyakarta'),
        ('JTI','Jawa Timur'),
    )
    provinsi = models.CharField(
        max_length=3,
        choices= PROVINSI_CHOICES,
        default='JKT',
    )

    def __str__(self):
        return self.get_provinsi_display()

class Pengumuman(models.Model):
    PRIORITAS_CHOICES = (
        ('00','Urgent'),
        ('01','Tinggi'),
        ('02','Normal'),
        ('03','Rendah'),
    )
    judul = models.CharField(max_length = 50, blank=False)
    prioritas = models.CharField(
        max_length=2,
        choices= PRIORITAS_CHOICES,
        default='02',
    )
    pemerintah = models.ForeignKey("Pemerintah", default=None, on_delete=models.CASCADE)
    isi_pengumuman = models.TextField(blank=False)

    def __str__(self):
        return '{}-{}-{}'.format(self.judul[:15], self.get_prioritas_display(), str(self.pemerintah))
    
    class Meta:
        ordering = ['prioritas']
        unique_together = ['judul', 'prioritas', 'pemerintah']