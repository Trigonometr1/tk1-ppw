from django.db import IntegrityError, transaction
from django.test import TestCase, SimpleTestCase, Client
from .models import Pemerintah, Pengumuman

# Create your tests here.
class PostPengumumanTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.client = Client()
        cls.pemerintah_jkt = Pemerintah.objects.create(provinsi='JKT')
        cls.pengumuman_1 = Pengumuman.objects.create(judul='Sebuah pengumuman', prioritas='00', pemerintah=cls.pemerintah_jkt, isi_pengumuman='Lorem ipsum dolor sit amet')
        return super().setUpTestData()

    def test_url_exist(self):
        response = self.client.get('/post-pengumuman/')
        self.assertEqual(response.status_code,200)
        
        # based on existing pemerintah object
        response = self.client.get('/post-pengumuman/JKT/')
        self.assertEqual(response.status_code,200)

    def test_template_loaded(self):
        response = self.client.get('/post-pengumuman/')
        self.assertTemplateUsed(response, 'post_pengumuman/post_home.html')
        self.assertTemplateUsed(response, 'base.html')

        # based on existing pemerintah object
        response = self.client.get('/post-pengumuman/JKT/')
        self.assertTemplateUsed(response, 'post_pengumuman/post_form.html')
        self.assertTemplateUsed(response, 'base.html')
    
    def test_model_and_str_method_pemerintah(self):
        self.assertTrue(Pemerintah.objects.count() == 1)
        self.assertTrue(Pemerintah.objects.get(provinsi='JKT') in Pemerintah.objects.all())
        self.assertEqual(str(self.pemerintah_jkt),'DKI Jakarta')

    def test_model_and_str_method_pengumuman(self):
        self.assertEqual(str(self.pengumuman_1),'Sebuah pengumum-Urgent-DKI Jakarta')

    def test_pemerintah_object_is_created(self):
        response = self.client.get('/post-pengumuman/BAN/')
        self.assertEquals(str(Pemerintah.objects.get(provinsi='BAN')),'Banten')

    def test_pengumuman_object_is_created_and_url_redirected(self):
        response = self.client.post('/post-pengumuman/JKT/', {
            'judul':'Pengumuman untuk Jakarta', 
            'prioritas':'00', 
            'pemerintah':self.pemerintah_jkt, 
            'isi_pengumuman':'ini isinya',
            })
        self.assertEquals(response.status_code, 302)
        self.assertTrue(Pengumuman.objects.get(judul='Pengumuman untuk Jakarta') in Pengumuman.objects.all())
    
    def test_duplicate_pengumuman_object_is_not_created(self):
        with self.assertRaises(IntegrityError):
            # roll back transaction before IntegrityError
            with transaction.atomic():
                response = self.client.post('/post-pengumuman/JKT/', data={
                    'judul':'Sebuah pengumuman', 
                    'prioritas':'00',
                    'pemerintah':self.pemerintah_jkt, 
                    'isi_pengumuman':'ini isinya',
                    })
                raise IntegrityError
        self.assertRedirects(response, '/post-pengumuman/JKT/')

    def test_post_pengumuman_sukses_and_redirect(self):
        response = self.client.post('/post-pengumuman/JKT/', {
            'judul':'Pengumuman untuk Jakarta', 
            'prioritas':'00', 
            'pemerintah':self.pemerintah_jkt, 
            'isi_pengumuman':'ini isinya',
            }, follow=True)
        self.assertRedirects(response, '/post-pengumuman/JKT/sukses', fetch_redirect_response=True)
        isi_template_sukses = response.content.decode('utf-8')
        self.assertIn('Pengumuman untuk Provinsi DKI Jakarta berhasil ditambahkan. Kembali ke halaman utama untuk melihatnya.', isi_template_sukses)
