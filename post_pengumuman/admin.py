from django.contrib import admin
from .models import Pemerintah, Pengumuman

# Register your models here.
admin.site.register(Pengumuman)
admin.site.register(Pemerintah)
