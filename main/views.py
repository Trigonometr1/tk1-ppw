from django.views.generic import ListView
from django.shortcuts import render
from post_pengumuman.models import Pengumuman
from tips.models import tips

# def home(request):
#     tip = tips.objects.all()
#     return render(request, 'main/home.html',{'tips': tip})

class HomeView(ListView):
    template_name = 'main/home.html'
    context_object_name = 'pengumuman_list'

    def get_queryset(self):
        return Pengumuman.objects.order_by('prioritas')

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context.update({
            'tips_list':tips.objects.all(),
        })
        return context
